<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <link href="https://fonts.googleapis.com/css?family=Public+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styleexo12.css">
</head>    

    <?php
    
      /* 
    *   Simuler le tableau de bord d'une déchetterie grâce à la puissance de calcul de PHP et l'affichage du CSS
    *   Ici, votre tableau de bord, permettera de visualisé les stocks de dechets ( en tonne ) de la communauté de commune
    *   Trois catégories dans le tri : cartons, déchets verts, mobilier. La fonction rand affectera une quantité aléatoire aux  
    *   déchetteries du réseaux, à vous de faire en sorte que l'on sache qui à quoi et combien cela represente en tout. Préciser en 
    *   nombre de camion(s) le volume que cela represente. Un camion = 30 tonnes.
    *   Police utiliser : Public Sans sur Google Font, effet de ligne plus grise au survol de la ligne 
    */
    
    
    $lev = "Loir en vallée";
    $lcsll = "La Chartre-sur-le Loir";
    $bsd = "Beaumont-sur-Dême";
    $m = "Marçon";
    $c = "Chahaignes";
    $l = "Lhomme";
    
    $carton = rand(0,20);
    $vert = rand(0,20);
    $mobilier = rand(0,20);

    $txcarton = $carton*6;
    $txvert = $vert*6;
    $txmob = $mobilier*6;

    $nbcc = round ($txcarton/30);
    $nbcv = round ($txvert/30);
    $nbcm = round ($txmob/30);

  

    $nbtxcamion = $nbcc+$nbcv+$nbcm;
        
    ?>
   
    <!-- écrire le code après ce commentaire -->
    <div id="titre">
      <h1>SICTOM de Montoire / La Chartre</h1>
      <h2>Quantité total de déchets dans la Communauté de communes</h2> 
    </div>
      <section>
        <div id="carton" class="card">
          <img src="carton.svg" alt="carton">
          <p><?php echo $txcarton ?></p>
          <p>Besoin de <?php echo $nbcc ?> camion(s)</p>
        </div>
        <div id="dechet" class="card">
          <img src="vert.svg" alt="pomme">
          <p><?php echo $txvert ?></p>
          <p>Besoin de <?php echo $nbcv ?> camion(s)</p>
        </div>
        <div id="etoile" class="card">
          <img src="mobilier.svg" alt="ecran d'oridnateur">
          <p><?php echo $txmob ?></p>
          <p>Besoin de <?php echo $nbcm ?> camion(s)</p>
        </div>
      </section>

      <div id="result">
        <p>Nombre de camion(s) total à prévoir : <?php echo round ($nbtxcamion, 0)?></p>
      </div>

      <table cellspacing=0>
          <thead>
            <tr>
              <th>Site de collecte</th>
              <th><img src="carton.svg" alt="carton"></th>
              <th><img src="vert.svg" alt="pomme"></th>
              <th><img src="mobilier.svg" alt="ecran d'oridnateur"></th>
          </tr>
          </thead>
          <tbody>
            <tr>
              <td>Loir en vallée</td>
              <td><?php echo $carton ?></td>
              <td><?php echo $vert ?></td>
              <td><?php echo $mobilier ?></td>
          </tr>
            <tr>
              <td>La Chartre-sur-le Loir</td>
              <td><?php echo $carton ?></td>
              <td><?php echo $vert ?></td>
              <td><?php echo $mobilier ?></td>
          </tr>
            <tr>
              <td>Beaumont-sur-Dême</td>
              <td><?php echo $carton ?></td>
              <td><?php echo $vert ?></td>
              <td><?php echo $mobilier ?></td>
          </tr>
            <tr>
              <td>Marçon</td>
              <td><?php echo $carton ?></td>
              <td><?php echo $vert ?></td>
              <td><?php echo $mobilier ?></td>
          </tr>
            <tr>
              <td>Chahaignes</td>
              <td><?php echo $carton ?></td>
              <td><?php echo $vert ?></td>
              <td><?php echo $mobilier ?></td>
          </tr>
            <tr>
              <td>Lhomme</td>
              <td><?php echo $carton ?></td>
              <td><?php echo $vert ?></td>
              <td><?php echo $mobilier ?></td>
          </tr>
          </tbody>
      </table>
    
    
  
    <!-- écrire le code avant ce commentaire -->

</body>
</html>


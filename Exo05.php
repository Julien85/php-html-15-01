<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <style>
        .container-1{
            width: 90%;
            margin: 0 auto;
            display: flex;
            justify-content: space-around;
        }
        
        .bloc-1 {
            font-family: "Arial";
            background-color: blue;
            width: 40%;
            color:white;
            text-align: center;
            border-radius:10px;
            box-shadow: 0 2px 5px grey;
        }
        
         .container-2 {
            width: 50%;
             margin: 0 auto;
            display: flex;
            flex-direction: column;
            justify-content: center;
        }
        
        .bloc-2 {
            font-family: "Arial";
            background-color: black;
            width: 100%;
            margin: 10px;
            color:white;
            text-align: center;
            border-radius:10px;
            box-shadow: 0 2px 5px grey;
        }
    </style>
</head>    

    <?php
    
    // La variable $nombre est comprise entre 0 et 10 aléatoirement.
    // Avec php, afficher la class container-1 et bloc-1 si $nombre est inferieur ou = à 5. Sinon
    // appliquer la class container-2 et bloc-2
    // Afin d'avoir un code plus clair, utiliser la fonction include pour simplifier la lecture du code
    
    $nombre = rand(0,10);
    
    ?>
    
    <!-- écrire le code après ce commentaire -->
    <?php
        echo "la valeur de $nombre <br>";

        if($nombre <=5){
            echo "<div class= container-1> 
                    <div class= bloc-1><p>Ceci est un test</p></div>
                    <div class= bloc-1><p>Ceci est un test</p></div>
                </div>";
                    
        }else{
            echo "<div class= container-2>
                    <div class= bloc-2><p>Ceci est un test</p></div>
                    <div class= bloc-2><p>Ceci est un test</p></div>
                 </div>";
                        
        }


?>

    
    <!-- écrire le code avant ce commentaire -->

</body>
</html>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // Ecrivez un script qui affiche les multiples du nombre 3 inférieurs à un nombre donné.
    // Résultat : 
    // les multiples du nombre 3 infèrieur à 20 :
    // 3
    // 6
    // 9
    // 12
    // 15
    // 18

    
    
    ?>
    
    <!-- écrire le code après ce commentaire -->

    <?php
        $j = rand(10, 50);

        echo "les multiples de 3 inférieur à 50 <br>";

        for($i = 3; $i<$j ; $i++){
            if ($i%3==0){
                echo "$i" . '<br>';
            }
        }
    ?>

    <!-- écrire le code avant ce commentaire -->

</body>
</html>

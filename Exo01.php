<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
    
    <?php
    
    // Grâce à la fonction echo, faites apparaitre les balises html suivantes : h1, section, div, p 
    // Vous mettrez un texte cohérent dedans pour afficher une belle page sur votre navigateur
    // /!\ ATTENTION /!\ PHP ne doit qu'afficher les balises, le texte sera écrit en HTML
    
    ?>
    
    <!-- écrire le code après ce commentaire -->
    <?php
        echo "<h1>Coucou</h1>
            <section>Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati, eaque?</section>
            <div>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad, vero?</div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, mollitia.</p>";

?>
    
    
    <!-- écrire le code avant ce commentaire -->

</body>
</html>

